create database demo;

create table public.contact(
	id serial not null,
	contact_name character varying(40) not null,
	first_name character varying(40),
	last_name character varying(40),
	constraint pk_contact_id primary key (id)
);

insert into public.contact (contact_name) values ('one');
insert into public.contact (contact_name) values ('two');

select * from public.contact