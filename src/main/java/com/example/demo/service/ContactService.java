package com.example.demo.service;

import com.example.demo.domain.Contact;

import java.util.List;

/**
 * Created by Ikomang_M on 12/21/2016.
 */
public interface ContactService {

    List<Contact> findAll();
}
