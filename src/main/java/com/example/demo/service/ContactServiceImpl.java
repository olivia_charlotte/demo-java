package com.example.demo.service;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Contact;
import com.example.demo.repository.ContactRepository;

/**
 * Created by Ikomang_M on 12/21/2016.
 */
@Service
public class ContactServiceImpl implements ContactService {

    private final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);

    @Inject
    private ContactRepository contactRepository;

    @Override
    public List<Contact> findAll() {
        log.debug("Service request findAll");
        return contactRepository.findAll();
    }
}
