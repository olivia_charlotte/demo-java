package com.example.demo.aws;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.example.demo.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by Ikomang_M on 12/23/2016.
 */
@Service
public class AwsS3ServiceImpl implements AwsS3Service {

    private static final Logger log = LoggerFactory.getLogger(AwsS3ServiceImpl.class);

    @Inject
    private ApplicationProperties applicationProperties;

    private AmazonS3 getAmazonS3Client() {
        log.debug("Initializing Amazon S3 Client");
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(applicationProperties.getAws().getAccessKeyId(), applicationProperties.getAws().getSecretKey());
        return AmazonS3ClientBuilder.standard()
                .withRegion(applicationProperties.getAws().getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public void uploadFile(String bucketName, String filePath) {
        String key_name = Paths.get(filePath).getFileName().toString();
        getAmazonS3Client().putObject(bucketName, key_name, filePath);
    }

    /*public void deleteFile(String key) {
        final AmazonS3 s3 = new AmazonS3Client();
        try {
            s3.deleteObject(bucketName, key);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
    }*/

    /*public void moveFile() {
        final AmazonS3 s3 = new AmazonS3Client();
        try {
            s3.copyObject(from_bucket, object_key, to_bucket, object_key);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
    }*/

    @Override
    public String getFileUrlPath(String bucketName, String key) {
        log.debug("getFileUrlPath bucketName: {}, key: {}", bucketName, key);
        URL url = getAmazonS3Client().generatePresignedUrl(bucketName, key, null);
        return url.toString();
    }
}
