package com.example.demo.aws;

/**
 * Created by Ikomang_M on 12/23/2016.
 */
public interface AwsS3Service {

    String getFileUrlPath(String bucketName, String key);

}
