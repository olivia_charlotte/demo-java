package com.example.demo.aws;

import com.google.common.collect.ImmutableSet;

/**
 * Created by Ikomang_M on 12/23/2016.
 */
public final class AwsS3ContentType {

    public static final String PNG = "image/png";
    public static final String JPG = "image/jpg";
    public static final String JPEG = "image/jpeg";
    public static final String PDF = "application/pdf";
    public static final String ZIP = "application/zip";

    public static final ImmutableSet<String> AwsS3ContentTypes = ImmutableSet.of(
            PNG, JPG, JPEG, PDF, ZIP);
}
