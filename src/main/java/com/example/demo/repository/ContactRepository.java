package com.example.demo.repository;

import com.example.demo.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ikomang_M on 12/21/2016.
 */
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
