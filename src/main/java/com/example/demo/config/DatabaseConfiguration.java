package com.example.demo.config;

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Ikomang_M on 12/22/2016.
 */

@Configuration
@EnableJpaRepositories("com.example.demo.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {

    @Bean
    public Hibernate5Module hibernate5Module() {
        return new Hibernate5Module();
    }
}
