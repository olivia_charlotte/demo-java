package com.example.demo.config;

/**
 * Created by Ikomang_M on 12/22/2016.
 */
public final class Constants {

    private Constants() {
    }

    // Spring profiles for development, test and production
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_TEST = "test";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";

}
