package com.example.demo.web.rest;

import com.example.demo.aws.AwsS3Service;
import com.example.demo.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * Created by Ikomang_M on 12/23/2016.
 */
@RestController
@RequestMapping("/api")
public class AwsS3Resource {

    private final Logger log = LoggerFactory.getLogger(AwsS3Resource.class);

    @Inject
    private AwsS3Service awsS3Service;

    @Inject
    ApplicationProperties applicationProperties;

    @PostMapping("/s3/url")
    public String getFileUrlPath(@RequestBody String key) {
        log.info("REST request to getFileUrlPath key: {}", key);
        String url = awsS3Service.getFileUrlPath(applicationProperties.getAws().getBucketName(), key);
        return url;
    }
}
