package com.example.demo.web.rest;

import com.example.demo.domain.Contact;
import com.example.demo.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Ikomang_M on 12/21/2016.
 */
@RestController
@RequestMapping("/api")
public class ContactResource {

    private final Logger log = LoggerFactory.getLogger(ContactResource.class);

    @Inject
    private ContactService contactService;

    @GetMapping("/contacts")
    public List<Contact> findAll() {
        log.info("REST request to findAll Contact");
        return contactService.findAll();
    }
}
